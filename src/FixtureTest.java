import static org.junit.Assert.*;
import jl.stryktipsgenerator.model.Fixture;

import org.junit.Test;


public class FixtureTest {

	@Test
	public void testGenerateSignNoHome() {
		Fixture f = new Fixture(0,3,2);
		
		for(int i = 0; i < 10000;i++)
			assertFalse(f.generateSign().equals("1"));
		
	}

	@Test
	public void testGenerateSignNoAway() {
		Fixture f = new Fixture(1,3,0);
		
		for(int i = 0; i < 10000;i++)
			assertFalse(f.generateSign().equals("2"));
		
	}
	
	@Test
	public void testGenerateSignNoDraw() {
		Fixture f = new Fixture(1,0,2);
		
		for(int i = 0; i < 10000;i++)
			assertFalse(f.generateSign().equals("X"));
		
	}
	
}
