import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.swing.text.html.MinimalHTMLWriter;

import jl.stryktipsgenerator.model.BetSlip;

public class MainWindow {

	private static final String exitString = "exit";
	private static BetSlip betSlip;
	private static int minVote = 0;
	private static InputStream inputStream = System.in;
	private static boolean cancel = false;
	private static int accumulativeResult;

	private static SortedMap<Integer,Integer> correctRowsCounters = new TreeMap<Integer,Integer>();
	private static int maxWin = 13;
	private static int minWin = 10;
	private static int totalPayout = 0;
	
	/**
	 * Launch the application.
	 * 
	 * @throws IOException
	 *             if it cannot read from command line
	 */
	public static void main(String[] args) throws IOException {

		System.out.println("Arguments:");
		for (String a : args)
			System.out.print(a + " ");

		System.out.println("");

		handleArguments(args);

		BufferedReader reader = new BufferedReader(new InputStreamReader(
				inputStream));
		for(int i = maxWin; i >= minWin;i++)
			correctRowsCounters.put(i, 0);
		while (!cancel) {
			betSlip  = new BetSlip();
			if (readVotes(reader)) {
				createBetSlip();

				if (readCorrectResults(reader)) {
					betSlip.calculateNumberOfCorrectRows();

					if (readPayouts(reader)) {
						betSlip.calculatePayouts();
						writePotentialPayout();
					}
				}
			}
		}
		writeTotalPayout();
	}

	private static void writeTotalPayout() {
		for (int i = maxWin; i >= minWin; i++)
		{		
			System.out.println("Total N of " + i +": " + correctRowsCounters.get(i));
		}
		System.out.println("Total Payout: " + totalPayout);
	}

	private static void writePotentialPayout() {
		for (int i = maxWin; i >= minWin; i++)
		{
			int n = betSlip.getNumberOfCorrectRows(i);
			System.out.println("N of " + i +": " + n);
			correctRowsCounters.put(i, correctRowsCounters.get(i) + n);
		}
		
		int payout = betSlip.getPayout();
		System.out.println("Payout: " + payout);
		totalPayout += payout;
	}

	private static boolean readPayouts(BufferedReader reader) throws IOException {
		System.out.println("Enter the payouts of the bet slip");

		for (int i = betSlip.getBestCorrectCount(); i >= 10; i--) {
			System.out.println(i + ": ");
			System.out.flush();

			String input = reader.readLine();
			if(input == null){
				cancel = true;
				return false;
			}

			betSlip.setPayout(i, Integer.parseInt(input));
		}
		return true;
	}

	private static boolean readCorrectResults(BufferedReader reader)
			throws IOException {
		System.out
				.println("Enter the correct result (1, X or 2) for each match, "
						+ System.lineSeparator()
						+ "each match separated by a new line:");

		for (int i = 0; i < 13; i++) {
			String input = reader.readLine();
			if (input == null) {
				cancel = true;
				return false;
			}
			input = input.toUpperCase();
			String correctSign = "";
			if (input.contains("1"))
				correctSign = "1";
			else if (input.contains("X"))
				correctSign = "X";
			else if (input.contains("2"))
				correctSign = "2";

			if (correctSign.equalsIgnoreCase(""))
				throw new IllegalArgumentException(
						"Input was not valid, line must contain 1, X or 2");

			betSlip.setFixtureSign(i, correctSign);
		}
		return true;
	}

	private static void createBetSlip() throws IOException {
		betSlip.generateRows();
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd.HH.mm.ss");
		Date date = new Date();
		String fileName = dateFormat.format(date) + ".txt";
		FileWriter writer = new FileWriter(fileName);
		List<String> rows = betSlip.getRows();
		writer.write("Stryktipset");
		for (String r : rows) {
			writer.write(System.lineSeparator());
			writer.write(r);
		}
		writer.close();

		System.out.println("Bet slip written to file: " + fileName);
		System.out.println("Total number of possible combinations: "
				+ betSlip.getMaxCombinationsCount());
		System.out.println("Generated number of rows: "
				+ betSlip.getGeneratedCount());
		System.out.println("Accepted number of rows: "
				+ betSlip.getAcceptedCount());
	}

	private static boolean readVotes(BufferedReader reader) throws IOException,
			IllegalArgumentException, NumberFormatException {
		System.out.println("Enter the integer vote count for each match, "
				+ System.lineSeparator()
				+ "each vote separated by a whitespace,"
				+ System.lineSeparator()
				+ "and each match separated by a new line:");

		for (int i = 0; i < 13; i++) {
			String input = reader.readLine();
			if (input == null) {
				cancel = true;
				return false;
			}
			String[] vals = input.split("\\s+");

			if (vals.length != 3)
				throw new IllegalArgumentException(
						"Input votes must be three values");

			int homeVote = parseVote(vals[0]);
			int drawVote = parseVote(vals[1]);
			int awayVote = parseVote(vals[2]);

			betSlip.setFixture(i, homeVote, drawVote, awayVote);
		}

		return true;
	}

	private static int parseVote(String string) {
		int vote = Integer.parseInt(string);
		if (vote < minVote)
			return 0;
		else
			return vote;
	}

	private static void handleArguments(String[] args)
			throws NumberFormatException {
		for (int i = 0; i < args.length; i++) {
			if (args[i].equalsIgnoreCase("-help")) {
				printHelp();
			} else if (args[i].equalsIgnoreCase("-homeMin")) {
				betSlip.setHomeMin(Integer.parseInt(args[++i]));
			} else if (args[i].equalsIgnoreCase("-homeMax")) {
				betSlip.setHomeMax(Integer.parseInt(args[++i]));
			} else if (args[i].equalsIgnoreCase("-drawMin")) {
				betSlip.setDrawMin(Integer.parseInt(args[++i]));
			} else if (args[i].equalsIgnoreCase("-drawMax")) {
				betSlip.setDrawMax(Integer.parseInt(args[++i]));
			} else if (args[i].equalsIgnoreCase("-awayMin")) {
				betSlip.setAwayMin(Integer.parseInt(args[++i]));
			} else if (args[i].equalsIgnoreCase("-awayMax")) {
				betSlip.setAwayMax(Integer.parseInt(args[++i]));
			} else if (args[i].equalsIgnoreCase("-numberOfRows")) {
				betSlip.setNumberOfRows(Integer.parseInt(args[++i]));
			} else if (args[i].equalsIgnoreCase("-minVote")) {
				minVote = Integer.parseInt(args[++i]);
			} else if (args[i].equalsIgnoreCase("-file")) {
				try {
					inputStream = new FileInputStream(args[++i]);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					System.out.println("Could not find file: " + args[i]);
					System.exit(1);
				}
			}
		}
	}

	private static void printHelp() {
		StringBuilder sb = new StringBuilder();

		sb.append("-help\t\tDisplays this help");
		sb.append(System.lineSeparator());

		sb.append("-homeMin n\t\tSets the min number of home wins on a bet slip to n");
		sb.append(System.lineSeparator());

		sb.append("-homeMax n\t\tSets the max number of home wins on a bet slip to n");
		sb.append(System.lineSeparator());

		sb.append("-drawMin n\t\tSets the min number of draws on a bet slip to n");
		sb.append(System.lineSeparator());

		sb.append("-drawMax n\t\tSets the max number of draws on a bet slip to n");
		sb.append(System.lineSeparator());

		sb.append("-awayMin n\t\tSets the min number of away wins on a bet slip to n");
		sb.append(System.lineSeparator());

		sb.append("-awayMax n\t\tSets the max number of away wins on a bet slip to n");
		sb.append(System.lineSeparator());

		sb.append("-numberOfRows n\t\tSets the max number of generated combinations on the final bet slip to n");
		sb.append(System.lineSeparator());

		sb.append("-file [file path]\t\tmake the program read votes, correct bet slip and payouts from a file instead");
		sb.append(System.lineSeparator());

		System.out.println(sb.toString());
	}

	public MainWindow() {
	}

}
