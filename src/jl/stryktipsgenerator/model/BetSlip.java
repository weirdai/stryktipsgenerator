package jl.stryktipsgenerator.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class BetSlip {
	private int homeMin = 0;
	private int homeMax = 13;
	private int drawMin = 0;
	private int drawMax = 13;
	private int awayMin = 0;
	private int awayMax = 13;
	private int numberOfRows = 0;
	private int payout = 0;
	private HashSet<Row> generatedRows = new HashSet<Row>();
	private HashSet<Row> acceptedRows = new HashSet<Row>();
	private Fixture[] fixtureList = new Fixture[13];
	private Map<Integer, Integer> payoutMap = new HashMap<Integer, Integer>();
	private Map<Integer, Integer> numberOfCorrectMap = new HashMap<Integer, Integer>();

	public void setHomeMin(int homeMin) {
		this.homeMin = homeMin;
	}

	public void setHomeMax(int homeMax) {
		this.homeMax = homeMax;
	}

	public void setDrawMin(int drawMin) {
		this.drawMin = drawMin;
	}

	public void setDrawMax(int drawMax) {
		this.drawMax = drawMax;
	}

	public void setAwayMin(int awayMin) {
		this.awayMin = awayMin;
	}

	public void setAwayMax(int awayMax) {
		this.awayMax = awayMax;
	}

	public void setNumberOfRows(int numberOfRows) {
		this.numberOfRows = numberOfRows;
	}

	public void setFixture(int i, int homeCount, int drawCount, int awayCount) {
		fixtureList[i] = new Fixture(homeCount, drawCount, awayCount);
	}

	public void generateRows() {
		int maxNumberOfRows = CalculateMaxNumberOfRows();
		int acceptedLimit;
		if (numberOfRows > 0)
			acceptedLimit = Math.min(maxNumberOfRows, numberOfRows);
		else
			acceptedLimit = maxNumberOfRows;

		while (acceptedRows.size() < acceptedLimit
				&& generatedRows.size() < maxNumberOfRows)
			generateRow();
	}

	private void generateRow() {
		Row r = new Row();
		for (int i = 0; i < fixtureList.length; i++) {
			r.setFixture(i, fixtureList[i].generateSign());
		}
		if (generatedRows.add(r)) {
			if (r.Validate(homeMin, homeMax, drawMin, drawMax, awayMin, awayMax)) {
				acceptedRows.add(r);
			}
		}
	}

	private int CalculateMaxNumberOfRows() {
		int rowCount = 1;
		for (Fixture f : fixtureList) {
			rowCount *= f.getMultiplier();
		}
		return rowCount;
	}

	public List<String> getRows() {
		LinkedList<String> rows = new LinkedList<String>();
		for (Row r : acceptedRows)
			rows.add(r.toString());
		return rows;
	}

	public int getMaxCombinationsCount() {
		return CalculateMaxNumberOfRows();
	}

	public int getGeneratedCount() {
		return generatedRows.size();
	}

	public int getAcceptedCount() {
		return acceptedRows.size();
	}

	public void setFixtureSign(int i, String correctSign) {
		fixtureList[i].setCorrectSign(correctSign);
	}

	public void setPayout(int numberOfCorrectSigns, int payout) {
		payoutMap.put(numberOfCorrectSigns, payout);
	}

	public int getPayout() {
		return payout;
	}

	public void calculatePayouts() {
		calculatePayouts(numberOfCorrectMap, payoutMap);
	}

	private void calculatePayouts(Map<Integer, Integer> numberOfCorrectMap,
			Map<Integer, Integer> payoutMap) {
		for (Map.Entry<Integer, Integer> numberOfCorrect : numberOfCorrectMap
				.entrySet())
			if (payoutMap.containsKey(numberOfCorrect.getKey()))
				payout += numberOfCorrect.getValue()
						* payoutMap.get(numberOfCorrect.getKey());
	}

	public void calculateNumberOfCorrectRows() {
		for (Row r : acceptedRows) {
			int nCorrect = r.Correct(fixtureList);
			if (numberOfCorrectMap.containsKey(nCorrect))
				numberOfCorrectMap.put(nCorrect,
						numberOfCorrectMap.get(nCorrect) + 1);
			else
				numberOfCorrectMap.put(nCorrect, 1);
		}
	}

	public int getNumberOfCorrectRows(int i) {
		if (numberOfCorrectMap.containsKey(i))
			return numberOfCorrectMap.get(i);
		else
			return 0;
	}

	public int getBestCorrectCount() {
		int max = 0;
		
		for (Map.Entry<Integer, Integer> entry : numberOfCorrectMap.entrySet())
		{
			if(entry.getKey() > max)
				max = entry.getKey();
		}
		
		return max;
	}

}
