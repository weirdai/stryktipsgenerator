package jl.stryktipsgenerator.model;

import java.util.Random;

public class Fixture {
	private Random r = new Random();
	private int totalCount;
	private int homeCount;
	private int drawCount;
	private int awayCount;

	private String correctSign;
	
	public Fixture(int homeCount, int drawCount, int awayCount) {
		super();
		this.homeCount = homeCount;
		this.drawCount = drawCount;
		this.awayCount = awayCount;
		totalCount = homeCount + drawCount + awayCount;
	}

	public String generateSign() {
		int val = r.nextInt(totalCount);

		if (val < homeCount)
			return "1";
		else if (val < homeCount + drawCount)
			return "X";
		else if (val < homeCount + drawCount + awayCount)
			return "2";
		else
			return "error";
	}
	
	public void setCorrectSign(String correctSign) {
		this.correctSign = correctSign;
	}
	
	public String getCorrectSign()
	{
		return this.correctSign;
	}

	public int getMultiplier() {
		int m = 0;
		if (homeCount > 0)
			m++;
		if (drawCount > 0)
			m++;
		if (awayCount > 0)
			m++;
		return m;

	}

}
