package jl.stryktipsgenerator.model;

import java.util.HashSet;

public class Row {

	private HashSet<Integer> setFixtures = new HashSet<Integer>();
	private String[] row = new String[13];

	public Row() {

	}

	public void setFixture(int i, String sign) {
		if (!sign.equalsIgnoreCase("1") && !sign.equalsIgnoreCase("X")
				&& !sign.equalsIgnoreCase("2"))
			throw new IllegalArgumentException("sign");
		else {
			row[i] = sign;
			setFixtures.add(i);
		}
	}

	public boolean Validate(int homeMin, int homeMax, int drawMin, int drawMax,
			int awayMin, int awayMax) {
		return ValidateHome(homeMin, homeMax) && ValidateDraw(drawMin, drawMax)
				&& ValidateAway(awayMin, awayMax);
	}

	private boolean ValidateAway(int awayMin, int awayMax) {
		int count = 0;
		for (String sign : row)
			if (sign.equalsIgnoreCase("2"))
				count++;
		return count >= awayMin && count <= awayMax;
	}

	private boolean ValidateDraw(int drawMin, int drawMax) {
		int count = 0;
		for (String sign : row)
			if (sign.equalsIgnoreCase("X"))
				count++;
		return count >= drawMin && count <= drawMax;
	}

	private boolean ValidateHome(int homeMin, int homeMax) {
		int count = 0;
		for (String sign : row)
			if (sign.equalsIgnoreCase("1"))
				count++;
		return count >= homeMin && count <= homeMax;
	}

	@Override
	public String toString() {
		if (setFixtures.size() != 13)
			throw new IllegalStateException("All fixtures have not been set");

		StringBuilder sb = new StringBuilder();
		sb.append("E");
		for (String sign : row) {
			sb.append(",");
			sb.append(sign);
		}
		return sb.toString();
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof Row
				&& this.toString().equals(((Row) obj).toString());
	}
	
	@Override
	public int hashCode() {
		return this.toString().hashCode();
	}

	public int Correct(Fixture[] fixtureList) {
		int count = 0;
		for(int i = 0; i < row.length; i++)
			if(row[i].equalsIgnoreCase(fixtureList[i].getCorrectSign()))
				count++;
		
		return count;
	}
}
